//use std::time::Duration; // likely dont need
use mech_core::*;
use mech_utilities::*;
use std::thread::{self};
use crossbeam_channel::Sender;
use std::collections::HashMap;
use chat_gpt_lib_rs::{ChatGPTClient, ChatInput, Message, Model, Role};
use std::future::Future;

lazy_static! {
    static ref AI_CHATGPT: u64 = hash_str("ai/chatgpt");
    static ref PROMPT: u64 = hash_str("prompt");
    static ref KEY: u64 = hash_str("key");
    static ref RESPONSE: u64 = hash_str("response");
  }
  
  export_machine!(ai_chatgpt, ai_chatgpt_reg);
  
  extern "C" fn ai_chatgpt_reg(registrar: &mut dyn MachineRegistrar, outgoing: Sender<RunLoopMessage>, capability_token: &CapabilityToken) -> Result<String, MechError> {
    registrar.register_machine(Box::new(ChatGPT{outgoing}));
    Ok("#ai/chatgpt = [|prompt<string> key<string> response<string>|]".to_string())
  }
  
  #[derive(Debug)]
  pub struct ChatGPT {
    outgoing: Sender<RunLoopMessage>,
  }
  
  impl Machine for ChatGPT{
  
    fn name(&self) -> String {
      "ai/chatgpt".to_string()
    }
  
    fn id(&self) -> u64 {
      hash_str(&self.name())
    }
  
    fn on_change(&mut self, table: &Table) -> Result<(), MechError> {
        let value = table.get(&TableIndex::Index(1), &TableIndex::Alias(*PROMPT))?;
        let key = table.get(&TableIndex::Index(1), &TableIndex::Alias(*KEY))?;
        match (key,value) {
          (Value::String(api_key), Value::String(prompt)) => {
                let outgoing = self.outgoing.clone();
                let chatgpt_row = TableIndex::Index(1);
                let chatgpt_handle = thread::spawn(move || {
                    let base_url = "https://api.openai.com";
                    let client = ChatGPTClient::new(&api_key.to_string(), base_url);
                    let chat_input = ChatInput {
                        model: Model::Gpt3_5Turbo,
                        messages: vec![
                            Message {
                                role: Role::User,
                                content: prompt.to_string(),
                            },
                        ],
                        ..Default::default()
                    };

                    //let mut answer = client.chat(chat_input).await.unwrap();
                    let mut answer = tokio::runtime::Runtime::new().unwrap().block_on(async {
                        client.chat(chat_input).await.unwrap()
                    });
                    outgoing.send(RunLoopMessage::Transaction(vec![
                        Change::Set((*AI_CHATGPT,vec![(chatgpt_row.clone(), TableIndex::Alias(*RESPONSE), Value::String(MechString::from_string(answer.choices[0].message.content.clone())))]))
                    ]));
                    
                });
            }
            _ => return Err(MechError{msg: "".to_string(), id: 4782, kind: MechErrorKind::None}),
        }

         Ok(())
    }    
}
    


